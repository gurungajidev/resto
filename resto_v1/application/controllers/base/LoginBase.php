<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ApplicationBase extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// data
        $this->smarty->assign("config", $this->config);
        // load app data
        $this->base_load_app();
	}

	/*
	 * Method pengolah base load
	 * diperbolehkan untuk dioverride pada class anaknya
	 */

	protected function base_load_app() {
	    // load themes (themes default : default)
	    $this->smarty->load_themes("load-login.css");
	    // load javascript
	    $this->smarty->load_javascript("resource/js/script.js");
	    $this->smarty->load_javascript("resource/themes/adminlte/bower_components/jquery/dist/jquery.min.js");
	    $this->smarty->load_javascript("resource/themes/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js");
	    $this->smarty->load_javascript("resource/themes/adminlte/dist/js/adminlte.min.js");
	}

	/*
	 * Method layouting base document
	 * diperbolehkan untuk dioverride pada class anaknya
	 */

	protected function display($tmpl_name = 'base/document-login.html') {
	    // set template
	    $this->smarty->display($tmpl_name);
	}

}

/* End of file  */
/* Location: ./application/controllers/ */
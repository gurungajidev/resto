<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/KasirBase.php' );

class OrdersKasir extends ApplicationBase {

    public function __construct() {
        parent::__construct();
    }

	// homepage
	public function index() {
        // set template content
        $this->smarty->assign("template_content", "orderskasir/index.html");
		// output
		parent::display();
	}

}
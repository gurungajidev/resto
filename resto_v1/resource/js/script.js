// get cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split("; ");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
// reset all cookie
function resetCookie() {
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split("; ");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].split("=");
        document.cookie = c[0] + "=;";
    }
    // redirect
    window.location = base_url;
}
// get all cookie
let session = {};
function getAllCookie() {
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split("; ");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].split("=");
        session[c[0]] = c[1];
    }
}
getAllCookie();
/// cookie
function setCookie(name, value) {
    document.cookie = name + "=" + value + ";";
}
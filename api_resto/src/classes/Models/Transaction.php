<?php

namespace Api\Models;

class Transaction
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    // tampilkan semua orders
    public function get_all_transaction()
    {
        // sql
        $sql = "SELECT a.*, b.user_alias AS 'pelayan',
                GROUP_CONCAT(d.menu_id) AS 'menu_pesanan',
                GROUP_CONCAT(d.nama) AS 'nama_pesanan',
                GROUP_CONCAT(c.qty) AS 'jml_pesanan',
                GROUP_CONCAT(c.price) AS 'harga_pesanan',
                GROUP_CONCAT(c.note) AS 'note_pesanan',
                GROUP_CONCAT(c.total) AS 'total_pesanan'
                FROM orders a 
                INNER JOIN com_user b ON a.user_id = b.user_id
                LEFT JOIN order_detail c ON a.order_kode = c.order_kode
                LEFT JOIN order_menu d ON c.menu_id = d.menu_id
                GROUP BY a.order_kode
                ORDER BY a.request_time DESC";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute();
        // hasil
        $result = $stmt->fetchAll();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    // tampilkan semua menu ready
    public function get_list_menu_ready()
    {
        // sql
        $sql = "SELECT a.*
                FROM order_menu a
                WHERE a.status = 'ready'
                ORDER BY a.nama ASC";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute();
        // hasil
        $result = $stmt->fetchAll();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    // tampilkan semua order aktif
    public function get_all_active_orders()
    {
        // sql
        $sql = "SELECT a.*, b.user_alias AS 'pelayan',
                GROUP_CONCAT(d.menu_id) AS 'menu_pesanan',
                GROUP_CONCAT(d.nama) AS 'nama_pesanan',
                GROUP_CONCAT(c.qty) AS 'jml_pesanan',
                GROUP_CONCAT(c.price) AS 'harga_pesanan',
                GROUP_CONCAT(c.note) AS 'note_pesanan',
                GROUP_CONCAT(c.total) AS 'total_pesanan'
                FROM orders a 
                INNER JOIN com_user b ON a.user_id = b.user_id
                LEFT JOIN order_detail c ON a.order_kode = c.order_kode
                LEFT JOIN order_menu d ON c.menu_id = d.menu_id
                WHERE a.status = 'active'
                GROUP BY a.order_kode
                ORDER BY a.request_time DESC";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute();
        // hasil
        $result = $stmt->fetchAll();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    // kode terakhir order
    public function get_last_kode_order($prefix, $params)
    {
        // sql
        $sql = "SELECT RIGHT(order_kode, 3)'last_number'
                FROM orders
                WHERE order_kode LIKE :prefix
                ORDER BY order_kode DESC
                LIMIT 1";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute($params);
        // hasil
        $result = $stmt->fetch();
        if ($result) {
            // create next number
            $number = intval($result['last_number']) + 1;
            if ($number > 999) {
                return false;
            }
            $zero = '';
            for ($i = strlen($number); $i < 3; $i++) {
                $zero .= '0';
            }
            return $prefix . $zero . $number;
        } else {
            // create new number
            return $prefix . '001';
        }
    }

    // tambah orders
    public function insert_orders($params)
    {
        // sql
        $sql = "INSERT INTO orders (order_kode, user_id, no_meja, status, total, request_time, mdd, mdb) 
                VALUES (:order_kode, :user_id, :no_meja, :status, :total, NOW(), NOW(), :mdb)";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // tambah orders menu
    public function insert_orders_menu($params)
    {
        // sql
        $sql = "INSERT INTO order_detail (order_kode, menu_id, qty, price, total, note, mdd, mdb) 
                VALUES (:order_kode, :menu_id, :qty, :price, :total, :note, NOW(), :mdb)";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // ubah orders
    public function update_orders($params)
    {
        // sql
        $sql = "UPDATE orders 
                SET no_meja = :no_meja, total = :total,
                    mdd = NOW(), mdb = :mdb
                WHERE order_kode = :order_kode";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // hapus detail order
    public function delete_order_detail($params)
    {
        // sql
        $sql = "DELETE FROM order_detail 
                WHERE order_kode = :order_kode";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // selesaikan orderan
    public function finish_orders($params)
    {
        // sql
        $sql = "UPDATE orders 
                SET status = :status,
                    mdd = NOW(), mdb = :mdb
                WHERE order_kode = :order_kode";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // tampilkan semua history orders pelayan
    public function get_list_history_orders($params)
    {
        // sql
        $sql = "SELECT a.*, b.user_alias AS 'pelayan',
                GROUP_CONCAT(d.nama, ' ( ', c.qty, ' )') AS 'menu'
                FROM orders a 
                INNER JOIN com_user b ON a.user_id = b.user_id
                LEFT JOIN order_detail c ON a.order_kode = c.order_kode
                LEFT JOIN order_menu d ON c.menu_id = d.menu_id
                WHERE a.user_id = :user_id AND a.status = 'done'
                GROUP BY a.order_kode
                ORDER BY a.request_time DESC";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute($params);
        // hasil
        $result = $stmt->fetchAll();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

}
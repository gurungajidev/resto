<?php

namespace Api\Models;

class Login
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    // login user
    public function login_user($params, $password)
    {
        // sql
        $sql = "SELECT a.user_id, a.user_alias, a.user_gender, a.user_phone, a.user_pass,
                a.user_st, a.role
                FROM com_user a 
                WHERE a.user_name = :username 
                LIMIT 1";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute($params);
        // hasil
        $result = $stmt->fetch();
        if ($result) {
            // get user
            if ($result['user_pass'] === sha1($password)) {
                // reset semua token sebelumnya
                $this->reset_all_token(array('user_id' => $result['user_id']));
                // create token akses
                $result['token'] = sha1($result['user_id'] . "-" . time());
                // input login time dan akses token
                $new_params = array(
                    'user_id' => $result['user_id'],
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'token' => $result['token'],
                    'role' => $result['role'],
                    'expire' => date('Y-m-d H:i:s', strtotime('+7 day'))
                );
                $this->insert_login_time($new_params);
                // unset
                unset($result['user_pass']);
                // unset($result['role']);
                unset($result['user_st']);
                // cek authority then return id
                return $result;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    // insert login time
    public function insert_login_time($params)
    {
        // sql
        $sql = "INSERT INTO com_user_login (user_id, login_date, ip_address, token, role, expire) 
                VALUES (:user_id, NOW(), :ip_address, :token, :role, :expire)";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // update logout time
    public function update_logout_time($params)
    {
        // update logout
        $sql = "UPDATE com_user_login a SET logout_date = NOW(), token = NULL, expire = NULL, role = NULL 
                WHERE a.token = :token";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            // return
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // reset all token
    public function reset_all_token($params)
    {
        // update logout
        $sql = "UPDATE com_user_login a SET token = NULL, expire = NULL, role = NULL 
                WHERE a.user_id = :user_id";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            // return
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
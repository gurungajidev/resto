<?php

namespace Api\Models;

class Menu
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    // tampilkan semua menu
    public function get_list_all_menu()
    {
        // sql
        $sql = "SELECT *
                FROM order_menu
                ORDER BY nama ASC";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute();
        // hasil
        $result = $stmt->fetchAll();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    // tambah menu
    public function insert_menu($params)
    {
        // sql
        $sql = "INSERT INTO order_menu (nama, kategori, harga, status, mdd, mdb) 
                VALUES (:nama, :kategori, :harga, :status, NOW(), :mdb)";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return $this->container->get('db')->lastInsertId();
        } else {
            return FALSE;
        }
    }

    // ubah menu
    public function update_menu($params)
    {
        // sql
        $sql = "UPDATE order_menu 
                SET nama = :nama, kategori = :kategori, 
                    harga = :harga, status = :status,
                    mdd = NOW(), mdb = :mdb
                WHERE menu_id = :menu_id";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // tampilkan detail menu
    public function get_detail_menu($params)
    {
        // sql
        $sql = "SELECT *
                FROM order_menu
                WHERE menu_id = :menu_id";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        $stmt->execute($params);
        // hasil
        $result = $stmt->fetch();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    // hapus menu
    public function delete_menu($params)
    {
        // sql
        $sql = "DELETE FROM order_menu 
                WHERE menu_id = :menu_id";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // ubah status menu
    public function update_status_menu($params)
    {
        // sql
        $sql = "UPDATE order_menu 
                SET status = :status,
                    mdd = NOW(), mdb = :mdb
                WHERE menu_id = :menu_id";
        // eksekusi
        $stmt = $this->container->get('db')->prepare($sql);
        // return
        if ($stmt->execute($params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
<?php

namespace Api\Libraries;

class ImageManip {
    public function resize_image($file, $maxDim = 510) {
        list($width, $height, $type, $attr) = getimagesize( $file );
        if ( $width > $maxDim || $height > $maxDim ) {
            $target_filename = $file;
            $fn = $file;
            $size = getimagesize( $fn );
            $ratio = $size[0]/$size[1]; // width/height
            if( $ratio > 1) {
                $width = $maxDim;
                $height = $maxDim/$ratio;
            } else {
                $width = $maxDim*$ratio;
                $height = $maxDim;
            }
            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor( $width, $height );
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
            imagejpeg($dst, $target_filename); // adjust format as needed
        }
        return $file;
    }
}
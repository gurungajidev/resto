<?php

namespace Api\Libraries;

class Validation {

    private $fields, $input, $message, $rules, $errors, $errors_field;

    // init datas
    public function __construct($fields)
	{
		$this->fields = $fields;
        $this->input = array();
        $this->message = array();
        $this->rules = array();
        $this->errors = '';
        $this->errors_field = [];
	}

    // set rule pengecekan
    public function set_rules($input, $message, $rules)
    {
        array_push($this->input, $input);
        array_push($this->message, $message);
        array_push($this->rules, $rules);
    }

    // validasi dengan sesi
    public function run_validation()
    {
        foreach ($this->input as $index => $input) {
            // cek
            $rules = explode('|', $this->rules[$index]);
            // required
            if (in_array('required', $rules)) {
                if (isset($this->fields[$input]) && $this->fields[$input] != "") {
                    // skip
                } else {
                    $this->errors .= $this->message[$index] . " wajib diisi\n";
                }
            }
            // numeric
            if (in_array('numeric', $rules)) {
                if (isset($this->fields[$input]) && (bool) is_numeric($this->fields[$input])) {
                    // skip
                } else {
                    $this->errors .= $this->message[$index] . " harus angka\n";
                }
            }
            // enum
            $enum = stripos($this->rules[$index], 'enum/');  
            if ($enum > -1) {
                $check = explode(',', substr($this->rules[$index], $enum+5));
                if (!in_array($this->fields[$input], $check)) {
                    $this->errors .= $this->message[$index] . " harus " .substr($this->rules[$index], $enum+5). "\n";
                }
            }
        }
        if (!$this->errors) return TRUE;
        else return FALSE;
    }

    // validasi per field
    public function run_validation_field()
    {
        $errors = FALSE;
        foreach ($this->input as $index => $input) {
            // cek
            $rules = explode('|', $this->rules[$index]);
            // required
            if (in_array('required', $rules)) {
                if (isset($this->fields[$input]) && $this->fields[$input] != "") {
                    $this->errors_field[$input] = NULL;
                } else {
                    $this->errors_field[$input] = $this->message[$index] . " wajib diisi";
                    $errors = TRUE;
                }
            }
            // numeric
            if (in_array('numeric', $rules)) {
                if (isset($this->fields[$input]) && (bool) is_numeric($this->fields[$input])) {
                    $this->errors_field[$input] = NULL;
                } else {
                    $this->errors_field[$input] = $this->message[$index] . " harus angka";
                    $errors = TRUE;
                }
            }
            // enum
            $enum = stripos($this->rules[$index], 'enum/');  
            if ($enum > -1) {
                $check = explode(',', substr($this->rules[$index], $enum+5));
                if (!in_array($this->fields[$input], $check)) {
                    $this->errors_field[$input] = $this->message[$index] . " harus " .substr($this->rules[$index], $enum+5);
                    $errors = TRUE;
                } else {
                    $this->errors_field[$input] = NULL;
                }
            }
        }
        return !$errors;
    }

    // get error message
    public function get_error_message()
    {
        return $this->errors;
    }

    // get error message field
    public function get_error_message_field()
    {
        return $this->errors_field;
    }

}
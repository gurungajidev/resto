<?php

namespace Api\Controllers;

class TokenAkses
{
    protected $container;

    // konstruktor
    function __construct($container)
    {
        $this->container = $container;
    }

    function __invoke($request, $response, $next)
    {
        // header
        $token = $request->getHeader('token')?$request->getHeader('token')[0]:'-';
        // cek token akses
        $sql = "SELECT * 
                FROM com_user_login a
                WHERE a.token = :token 
                AND a.expire > NOW()
                LIMIT 1";
        $stmt = $this->container->get('db')->prepare($sql); 
        $stmt->execute(
            array(
                'token' => $token
            )
        );
        
        // cek akses
        $result = $stmt->fetch(); 
        if ($result) {
            return $next($request->withAttribute("token", $result), $response);
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'expire',
                        'message' => 'Token tidak diijinkan',
                        'data' => NULL
                    )
                );
        }
    }

    function pelayan($request, $response, $next)
    {
        // header
        $token = $request->getHeader('token')?$request->getHeader('token')[0]:'-';
        // cek token akses
        $sql = "SELECT * 
                FROM com_user_login a
                WHERE a.token = :token 
                AND a.expire > NOW()
                AND a.role = 'pelayan'
                LIMIT 1";
        $stmt = $this->container->get('db')->prepare($sql); 
        $stmt->execute(
            array(
                'token' => $token
            )
        );
        
        // cek akses
        $result = $stmt->fetch(); 
        if ($result) {
            return $next($request->withAttribute("token", $result), $response);
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'expire',
                        'message' => 'Token tidak diijinkan',
                        'data' => NULL
                    )
                );
        }
    }

    function kasir($request, $response, $next)
    {
        // header
        $token = $request->getHeader('token')?$request->getHeader('token')[0]:'-';
        $role = $request->getHeader('role')?$request->getHeader('role')[0]:'-';
        // cek token akses
        $sql = "SELECT * 
                FROM com_user_login a
                WHERE a.token = :token 
                AND a.expire > NOW()
                AND a.role = 'kasir'
                LIMIT 1";
        $stmt = $this->container->get('db')->prepare($sql); 
        $stmt->execute(
            array(
                'token' => $token
            )
        );
        
        // cek akses
        $result = $stmt->fetch(); 
        if ($result) {
            return $next($request->withAttribute("token", $result), $response);
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'expire',
                        'message' => 'Token tidak diijinkan',
                        'data' => NULL
                    )
                );
        }
    }
}
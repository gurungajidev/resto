<?php

namespace Api\Controllers;

class Login
{
    protected $container;
    protected $m_login;

    // konstruktor
    function __construct($container)
    {
        $this->container = $container;
        $this->m_login = $container->get('m_login');
    }

    // login user
    function login($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('username', 'Username', 'required');
        $validation->set_rules('password', 'Password', 'required');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // parameter
            $params = array(
                'username' => $request->getParsedBody()['username']
            );
            // cek login
            $login_user = $this->m_login->login_user($params, $request->getParsedBody()['password']);
            if ($login_user) {
                // return
                return $response->withJson(
                    array(
                        'status' => 'success',
                        'message' => "Berhasil",
                        'data' => $login_user
                    )
                );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => "Username / Password salah",
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // logout user
    function logout($request, $response, $args)
    {
        // parameter
        $params = array(
            'token' => $request->getAttribute('token')['token']
        );
        // logout user
        $this->m_login->update_logout_time($params);
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => "Berhasil",
                'data' => NULL
            )
        );
    }
}
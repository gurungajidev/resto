<?php

namespace Api\Controllers;

class Transaction
{
    protected $container;
    protected $m_transaction;

    // konstruktor
    function __construct($container)
    {
        $this->container = $container;
        $this->m_transaction = $container->get('m_transaction');
    }

    // daftar semua transaksi
    function get_all_transaction($request, $response, $args)
    {
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => 'Berhasil',
                'data' => $this->m_transaction->get_all_transaction()
            )
        );
    }

    // daftar semua menu yang siap disajikan
    function get_list_menu_ready($request, $response, $args)
    {
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => 'Berhasil',
                'data' => $this->m_transaction->get_list_menu_ready()
            )
        );
    }

    // daftar semua transaksi actif
    function get_all_active_orders($request, $response, $args)
    {
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => 'Berhasil',
                'data' => $this->m_transaction->get_all_active_orders()
            )
        );
    }

    // tambah order baru
    function add_order($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Pelayan', 'required');
        $validation->set_rules('no_meja', 'Nomor Meja', 'required|numeric');
        $validation->set_rules('total', 'Total Harga', 'required|numeric');
        $validation->set_rules('order', 'Menu', 'required');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // kode order
            $prefix = "ERP" . date('dmY') . "-";
            $order_kode = $this->m_transaction->get_last_kode_order($prefix, array('prefix' => $prefix . "%"));
            // params
            $params = array(
                'order_kode' => $order_kode,
                'user_id' => $request->getParsedBody()['user_id'],
                'no_meja' => $request->getParsedBody()['no_meja'],
                'status' => 'active',
                'total' => $request->getParsedBody()['total'],
                'mdb' => $request->getParsedBody()['user_id']
            );
            // cek insert orders
            if ($this->m_transaction->insert_orders($params)) {
                // menu yg diorder
                $rs_order = json_decode($request->getParsedBody()['order']);
                foreach ($rs_order as $order) {
                    // params
                    $params = array(
                        'order_kode' => $order_kode,
                        'menu_id' => $order->menu_id,
                        'qty' => $order->qty,
                        'price' => $order->price,
                        'total' => $order->total,
                        'note' => $order->note,
                        'mdb' => $request->getParsedBody()['user_id']
                    );
                    // tambahkan menu yg diorder
                    $this->m_transaction->insert_orders_menu($params);
                }
                // return
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => "Berhasil menambahkan pesanan",
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => "Gagal menambahkan pesanan",
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // ubah order
    function edit_order($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Pelayan', 'required');
        $validation->set_rules('order_kode', 'Kode Pesanan', 'required');
        $validation->set_rules('no_meja', 'Nomor Meja', 'required|numeric');
        $validation->set_rules('total', 'Total Harga', 'required|numeric');
        $validation->set_rules('order', 'Menu', 'required');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // params
            $params = array(
                'order_kode' => $request->getParsedBody()['order_kode'],
                'no_meja' => $request->getParsedBody()['no_meja'],
                'total' => $request->getParsedBody()['total'],
                'mdb' => $request->getParsedBody()['user_id']
            );
            // cek update orders
            if ($this->m_transaction->update_orders($params)) {
                // hapus order lama
                $where = array( 'order_kode' => $request->getParsedBody()['order_kode'] );
                $this->m_transaction->delete_order_detail($where);
                // menu yg diorder
                $rs_order = json_decode($request->getParsedBody()['order']);
                foreach ($rs_order as $order) {
                    // params
                    $params = array(
                        'order_kode' => $request->getParsedBody()['order_kode'],
                        'menu_id' => $order->menu_id,
                        'qty' => $order->qty,
                        'price' => $order->price,
                        'total' => $order->total,
                        'note' => $order->note,
                        'mdb' => $request->getParsedBody()['user_id']
                    );
                    // tambahkan menu yg diorder
                    $this->m_transaction->insert_orders_menu($params);
                }
                // return
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => "Berhasil mengubah pesanan",
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => "Gagal mengubah pesanan",
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // tutup order
    function close_order($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Pelayan', 'required');
        $validation->set_rules('order_kode', 'Kode Pesanan', 'required');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // params
            $params = array(
                'order_kode' => $request->getParsedBody()['order_kode'],
                'status' => 'cancel',
                'mdb' => $request->getParsedBody()['user_id']
            );
            // cek selesaikan orders cancel / done
            if ($this->m_transaction->finish_orders($params)) {
                // return
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => "Berhasil menutup pesanan",
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => "Gagal menutup pesanan",
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // selesaikan order
    function done_order($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Pelayan', 'required');
        $validation->set_rules('order_kode', 'Kode Pesanan', 'required');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // params
            $params = array(
                'order_kode' => $request->getParsedBody()['order_kode'],
                'status' => 'done',
                'mdb' => $request->getParsedBody()['user_id']
            );
            // cek selesaikan orders close / done
            if ($this->m_transaction->finish_orders($params)) {
                // return
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => "Berhasil menyelesaikan pembayaran",
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => "Gagal menyelesaikan pembayaran",
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // daftar riwayat transaksi pelayan
    function get_list_history_orders($request, $response, $args)
    {
        // argumen
        $user_id = isset($args['user_id']) ? $args['user_id'] : 0;
        $params = array( 'user_id' => $user_id );
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => 'Berhasil',
                'data' => $this->m_transaction->get_list_history_orders($params)
            )
        );
    }

}
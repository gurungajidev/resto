<?php

namespace Api\Controllers;

class ErrorDefault
{
    protected $container;

    // konstruktor
    function __construct($container)
    {
        $this->container = $container;
    }

    function __invoke($request, $response, $args)
    {
        return $response->withStatus(405)
            ->withJson(
                array(
                    'status' => 'error',
                    'message' => 'Fungsi tidak ditemukan',
                    'data' => NULL
                )
            );
    }
}
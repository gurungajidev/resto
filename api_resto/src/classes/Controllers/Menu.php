<?php

namespace Api\Controllers;

class Menu
{
    protected $container;
    protected $m_menu;

    // konstruktor
    function __construct($container)
    {
        $this->container = $container;
        $this->m_menu = $container->get('m_menu');
    }

    // daftar semua menu
    function get_list_all_menu($request, $response, $args)
    {
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => 'Berhasil',
                'data' => $this->m_menu->get_list_all_menu()
            )
        );
    }

    // daftar semua menu yang siap disajikan
    function get_list_menu_ready($request, $response, $args)
    {
        // return
        return $response->withJson(
            array(
                'status' => 'success',
                'message' => 'Berhasil',
                'data' => $this->m_menu->get_list_menu_ready()
            )
        );
    }

    // tambah menu
    function add_menu($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Kasir', 'required');
        $validation->set_rules('nama', 'Nama Menu', 'required');
        $validation->set_rules('kategori', 'Kategori Menu', 'required|enum/makanan,minuman,lain');
        $validation->set_rules('harga', 'Harga Menu', 'required|numeric');
        $validation->set_rules('status', 'Status Menu', 'required|enum/ready,not');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // parameter
            $params = array(
                'nama' => $request->getParsedBody()['nama'],
                'kategori' => $request->getParsedBody()['kategori'],
                'harga' => $request->getParsedBody()['harga'],
                'status' => $request->getParsedBody()['status'],
                'mdb' => $request->getParsedBody()['user_id'],
            );
            // cek insert
            if ($this->m_menu->insert_menu($params)) {
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => 'Menu berhasil ditambahkan',
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => 'Menu gagal ditambahkan',
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // ubah menu
    function edit_menu($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Kasir', 'required');
        $validation->set_rules('menu_id', 'ID Menu', 'required');
        $validation->set_rules('nama', 'Nama Menu', 'required');
        $validation->set_rules('kategori', 'Kategori Menu', 'required|enum/makanan,minuman,lain');
        $validation->set_rules('harga', 'Harga Menu', 'required|numeric');
        $validation->set_rules('status', 'Status Menu', 'required|enum/ready,not');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // parameter
            $params = array(
                'menu_id' => $request->getParsedBody()['menu_id'],
                'nama' => $request->getParsedBody()['nama'],
                'kategori' => $request->getParsedBody()['kategori'],
                'harga' => $request->getParsedBody()['harga'],
                'status' => $request->getParsedBody()['status'],
                'mdb' => $request->getParsedBody()['user_id'],
            );
            // cek update
            if ($this->m_menu->update_menu($params)) {
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => 'Menu berhasil diubah',
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => 'Menu gagal diubah',
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

    // hapus menu
    function delete_menu($request, $response, $args)
    {
        // params
        $menu_id = isset($args['menu_id']) ? $args['menu_id'] : 0;
        $menu = $this->m_menu->get_detail_menu(
            array(
                'menu_id' => $menu_id
            )
        );
        // return
        if ($menu) {
            // params
            $params = array( 'menu_id' => $menu_id ); 
            // cek hapus
            if ($this->m_menu->delete_menu($params)) {
                // return
                return $response->withJson(
                    array(
                        'status' => 'success',
                        'message' => 'Menu berhasil dihapus',
                        'data' => $menu_id
                    )
                );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => "Menu gagal dihapus",
                            'data' => NULL
                        )
                    );
            }
            
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => "Menu tidak ditemukan",
                        'data' => NULL
                    )
                );
        }
    }

    // ubah status menu
    function update_status($request, $response, $args)
    {
        // validation
        $validation = new \Api\Libraries\Validation($request->getParsedBody());
        // set rules
        $validation->set_rules('user_id', 'ID Kasir', 'required');
        $validation->set_rules('menu_id', 'ID Menu', 'required');
        $validation->set_rules('status', 'Status Menu', 'required|enum/ready,not');
        // validasi
        if ($validation->run_validation_field() == TRUE) {
            // parameter
            $params = array(
                'menu_id' => $request->getParsedBody()['menu_id'],
                'status' => $request->getParsedBody()['status'],
                'mdb' => $request->getParsedBody()['user_id'],
            );
            // cek update
            if ($this->m_menu->update_status_menu($params)) {
                return $response->withJson(
                        array(
                            'status' => 'success',
                            'message' => 'Menu berhasil diubah',
                            'data' => NULL
                        )
                    );
            } else {
                return $response->withStatus(405)
                    ->withJson(
                        array(
                            'status' => 'error',
                            'message' => 'Menu gagal diubah',
                            'data' => NULL
                        )
                    );
            }
        } else {
            return $response->withStatus(405)
                ->withJson(
                    array(
                        'status' => 'error',
                        'message' => $validation->get_error_message_field(),
                        'data' => NULL
                    )
                );
        }
    }

}
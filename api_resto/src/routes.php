<?php

use Slim\Http\Request;
use Slim\Http\Response;

/* ROUTE API */
$app->group('/api', function() {
    
    // default api route
    $this->get('', \Api\Controllers\ErrorDefault::class);
    
    // api login / logout
    $this->post('/login', \Api\Controllers\Login::class . ':login'); // login user
    $this->get('/logout', \Api\Controllers\Login::class . ':logout')->add(\Api\Controllers\TokenAkses::class); // logout user
    
    // api menu
    $this->group('/menu', function() {
        // semua menu
        $this->get('', \Api\Controllers\Menu::class . ':get_list_all_menu');
        // tambah menu
        $this->post('/add', \Api\Controllers\Menu::class . ':add_menu'); 
        // ubah menu
        $this->post('/edit', \Api\Controllers\Menu::class . ':edit_menu'); 
        // hapus menu
        $this->get('/delete/{menu_id}', \Api\Controllers\Menu::class . ':delete_menu'); 
        // ubah status menu
        $this->post('/update_status', \Api\Controllers\Menu::class . ':update_status'); 
    })->add(\Api\Controllers\TokenAkses::class.':kasir');
    
    // api transaksi
    $this->group('/trans', function() {

        // transaksi semua role
        $this->group('', function() {
            // semua transaksi aktif
            $this->get('/active', \Api\Controllers\Transaction::class . ':get_all_active_orders');
            // menu yg ready
            $this->get('/menu_ready', \Api\Controllers\Transaction::class . ':get_list_menu_ready');
            // ubah orderan
            $this->post('/edit', \Api\Controllers\Transaction::class . ':edit_order');
        })->add(\Api\Controllers\TokenAkses::class);

        // transaksi role kasir
        $this->group('', function() {
            // semua transaksi
            $this->get('', \Api\Controllers\Transaction::class . ':get_all_transaction');
            // tutup orderan
            $this->post('/close', \Api\Controllers\Transaction::class . ':close_order');
            // selesaikan pembayaran
            $this->post('/done', \Api\Controllers\Transaction::class . ':done_order');
        })->add(\Api\Controllers\TokenAkses::class . ':kasir');

        // transaksi role pelayan
        $this->group('', function() {
            // tambah orderan
            $this->post('/add', \Api\Controllers\Transaction::class . ':add_order');
            // riwayat order pelayan
            $this->get('/history/{user_id}', \Api\Controllers\Transaction::class . ':get_list_history_orders');
        })->add(\Api\Controllers\TokenAkses::class . ':pelayan');

    });

});
